CREATE TABLE bases(
  id            INTEGER PRIMARY KEY AUTOINCREMENT,
  segment_id    INTEGER,
  name          TEXT
);
