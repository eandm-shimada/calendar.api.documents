CREATE TABLE public_holidays (
  id         INTEGER PRIMARY KEY AUTOINCREMENT,
  segment_id INTEGER, -- 所属会社のセグメントID(公休用。祝日については0設定で、全セグメント共通とする)
  holiday    INTEGER, -- yyyyMMddの日付
  name       TEXT     -- 祝日／公休名
);
