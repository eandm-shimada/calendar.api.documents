CREATE TABLE auths(
  id         INTEGER PRIMARY KEY AUTOINCREMENT,
  user_id    INTEGER,
  email      TEXT,
  name       TEXT,
  provider   TEXT,
  uid        TEXT,
  token      TEXT
);
