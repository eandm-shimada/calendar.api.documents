CREATE TABLE segments(
  id            INTEGER PRIMARY KEY AUTOINCREMENT,
  name          TEXT
);
