CREATE TABLE workplaces(
  id            INTEGER PRIMARY KEY AUTOINCREMENT,
  segment_id    INTEGER,
  department_id INTEGER,
  name          TEXT
);
