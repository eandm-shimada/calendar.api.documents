CREATE TABLE users(
  id            INTEGER PRIMARY KEY AUTOINCREMENT,
  name          TEXT,
  segment_id    INTEGER,
  base_id       INTEGER,
  department_id INTEGER,
  workplace_id  INTEGER
);
