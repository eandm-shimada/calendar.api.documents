CREATE TABLE access_tokens(
  auth_id             INTEGER PRIMARY KEY,
  access_token        TEXT,
  access_token_limit  TEXT, -- yyyyMMdd hh:mm:ss
  refresh_token       TEXT,
  refresh_token_limit TEXT  -- yyyyMMdd hh:mm:ss
);
