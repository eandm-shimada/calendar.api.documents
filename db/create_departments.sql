CREATE TABLE departments(
  id            INTEGER PRIMARY KEY AUTOINCREMENT,
  segment_id    INTEGER,
  name          TEXT
);
